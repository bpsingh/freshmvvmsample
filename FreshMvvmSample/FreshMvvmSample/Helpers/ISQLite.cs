﻿using System;
using SQLite;
namespace FreshMvvmSample.Helpers
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();

    }
}
