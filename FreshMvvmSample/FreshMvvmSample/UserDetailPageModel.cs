﻿using System;
using System.Collections.ObjectModel;
using FreshMvvm;
using FreshMvvmSample.Models;
using FreshMvvmSample.Services;

namespace FreshMvvmSample
{
    public class UserDetailPageModel : FreshBasePageModel
    {
        public ContactInfo _contact;

        public IContactRepository _contactRepository;

        public UserDetailPageModel(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;

        }

        public override void Init(object initData)
        {
            _contact = (ContactInfo)initData;

        }


        public string Name
        {
            get => _contact.Name;
            set
            {
                _contact.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string MobileNumber
        {
            get => _contact.MobileNumber;
            set
            {
                _contact.MobileNumber = value;
                RaisePropertyChanged("MobileNumber");
            }
        }

        public string Age
        {
            get => _contact.Age;
            set
            {
                _contact.Age = value;
                RaisePropertyChanged("Age");
            }
        }

        public string Gender
        {
            get => _contact.Gender;
            set
            {
                _contact.Gender = value;
                RaisePropertyChanged("Gender");
            }
        }


        public string Address
        {
            get => _contact.Address;
            set
            {
                _contact.Address = value;
                RaisePropertyChanged("Address");
            }
        }
    }
}
