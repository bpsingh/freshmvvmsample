﻿using System;
using FreshMvvm;
using FreshMvvmSample.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FreshMvvmSample
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //Interface rgistration with FreshIOC   
            FreshIOC.Container.Register<IContactRepository, ContactRepository>();

            // The root page of your application   
            var mainPage = FreshPageModelResolver.ResolvePageModel<MainPageModel>();
            MainPage = new FreshNavigationContainer(mainPage);


        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
