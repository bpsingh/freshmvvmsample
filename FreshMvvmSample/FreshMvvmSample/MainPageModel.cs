﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using FreshMvvm;
using FreshMvvmSample.Models;
using FreshMvvmSample.Services;
using Xamarin.Forms;

namespace FreshMvvmSample
{
    public class MainPageModel: FreshBasePageModel
    {
        public ContactInfo _contact;

        public IContactRepository _contactRepository;
        public ICommand ItemTappedCommand { get; protected set; }



        public MainPageModel(IContactRepository contactRepository)
        {

            _contactRepository = contactRepository;

            ContactList = _contactRepository.GetAllContactsData();




            // Item Tapped
            ItemTappedCommand = new Command((object e) =>
            {

                if (e != null && e is ItemTappedEventArgs)
                {

                    var selectedItem = (ContactInfo)((ItemTappedEventArgs)e).Item;
                    ShowContactDetails(selectedItem);

                }
            });


        }







        public bool IsDataAvailable => _contactRepository.GetAllContactsData().Count > 0 ? true : false;


    
        async void ShowContactDetails(ContactInfo selectedContact)
        {
            await CoreMethods.PushPageModel<UserDetailPageModel>(selectedContact);
        }


        public string Name
        {
            get => _contact.Name;
            set
            {
                _contact.Name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string MobileNumber
        {
            get => _contact.MobileNumber;
            set
            {
                _contact.MobileNumber = value;
                RaisePropertyChanged("MobileNumber");
            }
        }

        public string Age
        {
            get => _contact.Age;
            set
            {
                _contact.Age = value;
                RaisePropertyChanged("Age");
            }
        }

        public string Gender
        {
            get => _contact.Gender;
            set
            {
                _contact.Gender = value;
                RaisePropertyChanged("Gender");
            }
        }

        public DateTime DOB
        {
            get => _contact.DOB;
            set
            {
                _contact.DOB = value;
                RaisePropertyChanged("DOB");
            }
        }

        public string Address
        {
            get => _contact.Address;
            set
            {
                _contact.Address = value;
                RaisePropertyChanged("Address");
            }
        }

        ObservableCollection<ContactInfo> _contactList;
        public ObservableCollection<ContactInfo> ContactList
        {
            get => _contactList;
            set
            {
                _contactList = value;
                RaisePropertyChanged("ContactList");
            }
        }
    }
}
