﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FreshMvvmSample.Models;
namespace FreshMvvmSample.Services
{
    public interface IContactRepository
    {
        ObservableCollection<ContactInfo> GetAllContactsData();

    }
}
