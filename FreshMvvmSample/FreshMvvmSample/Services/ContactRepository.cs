﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FreshMvvmSample.Models;

namespace FreshMvvmSample.Services
{
    public class ContactRepository : IContactRepository
    {
        public ContactRepository()
        {
        }

        public ObservableCollection<ContactInfo> GetAllContactsData()
        {

            var contactList = new ObservableCollection<ContactInfo>()
                    {
                        new ContactInfo(1,"BP SINGH","29","Male",System.DateTime.Now,"Hyderabad","+91 7028015027"),
                        new ContactInfo(2, "Varun","28", "Male", System.DateTime.Now, "Pune", "+91 7685478547"),
                        new ContactInfo(3, "Tariq","27", "Male", System.DateTime.Now, "Pune", "+91 7054856254"),
                        new ContactInfo(4, "Nitesh","30", "Male", System.DateTime.Now, "Lucknow", "+91 9875487512")
                    };
            return contactList;
        }

      
    }


}