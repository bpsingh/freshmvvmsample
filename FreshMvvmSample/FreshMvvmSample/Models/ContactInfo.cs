﻿using System;
namespace FreshMvvmSample.Models
{
    public class ContactInfo
    {
        public ContactInfo()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }

        public ContactInfo(int id, string name, string age, string gender, DateTime dOB, string address, string mobileNumber)
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Age = age ?? throw new ArgumentNullException(nameof(age));
            Gender = gender ?? throw new ArgumentNullException(nameof(gender));
            DOB = dOB;
            Address = address ?? throw new ArgumentNullException(nameof(address));
            MobileNumber = mobileNumber ?? throw new ArgumentNullException(nameof(mobileNumber));
        }
    }
}
