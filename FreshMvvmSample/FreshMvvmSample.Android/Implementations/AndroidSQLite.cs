﻿using System.IO;  
using SQLite;  
using FreshMvvmSample.Droid.Implementations;  
using FreshMvvmSample.Helpers;  
  
[assembly: Xamarin.Forms.Dependency(typeof(AndroidSQLite))]  
namespace FreshMvvmSample.Droid.Implementations
{
    public class AndroidSQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // Documents folder  
            var path = Path.Combine(documentsPath, DatabaseHelper.DbFileName);
            var plat = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
            new AndroidSQLite
            var conn = new SQLiteConnection(plat, path);

            // Return the database connection  
            return conn;
        }
    }
}
